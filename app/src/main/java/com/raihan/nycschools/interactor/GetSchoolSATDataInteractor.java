package com.raihan.nycschools.interactor;

import android.content.Context;
import android.os.AsyncTask;

import com.raihan.nycschools.contract.SchoolSATDataContract;
import com.raihan.nycschools.db.SchoolRoomDatabase;
import com.raihan.nycschools.model.School;
import com.raihan.nycschools.model.SchoolSATData;
import java.util.List;

public class GetSchoolSATDataInteractor implements SchoolSATDataContract.Interactor {

    @Override
    public void getSATData(OnFinishedListener onFinishedListener, Context context, String dbn) {
        new schoolSATDataFetchTask(onFinishedListener,context,dbn).execute();
    }

    class schoolSATDataFetchTask extends AsyncTask{
        private final OnFinishedListener onFinishedListener;
        private final Context context;
        private final String dbn;
        private List<SchoolSATData> schoolSATData;
        private List<School> schools;

        public schoolSATDataFetchTask(OnFinishedListener onFinishedListener, Context context, String dbn) {
            this.onFinishedListener = onFinishedListener;
            this.context = context;
            this.dbn = dbn;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            schoolSATData = SchoolRoomDatabase.getDatabase(context).schoolDao().findSchoolSATDataWithDbn(dbn);
            schools = SchoolRoomDatabase.getDatabase(context).schoolDao().findSchoolWithDbn(dbn);
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if(schoolSATData.size() != 0 ) {
                onFinishedListener.onFinished(schoolSATData.get(0), schools.get(0));
            } else{
                onFinishedListener.onFailure(schools.get(0));
            }
        }
    }
}
