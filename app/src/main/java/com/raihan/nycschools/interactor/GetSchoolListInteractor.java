package com.raihan.nycschools.interactor;

import android.content.Context;
import android.os.AsyncTask;

import com.raihan.nycschools.contract.SchoolListContract;
import com.raihan.nycschools.dataInterface.SchoolsDataService;
import com.raihan.nycschools.db.SchoolRoomDatabase;
import com.raihan.nycschools.model.School;
import com.raihan.nycschools.model.SchoolSATData;
import com.raihan.nycschools.network.RetrofitInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetSchoolListInteractor implements SchoolListContract.Interactor {
    @Override
    public void getSchoolList(final OnFinishedListener onFinishedListener, final Context context) {
        SchoolsDataService service = RetrofitInstance.getRetrofitInstance().create(SchoolsDataService.class);

        /** Call the method with parameter in the interface to get the school data*/
        Call<List<School>> call = service.getSchoolListData();

        call.enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                (new insertSchoolListTask(onFinishedListener,context,response)).execute();
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                (new cachedSchoolListTask(onFinishedListener,context,t)).execute();

            }
        });

        Call<List<SchoolSATData>> callSchoolSATDATA = service.getSchoolSATData();

        callSchoolSATDATA.enqueue(new Callback<List<SchoolSATData>>() {
            @Override
            public void onResponse(Call<List<SchoolSATData>> call, Response<List<SchoolSATData>> response) {
                (new insertSchoolSATDataTask(onFinishedListener,context,response)).execute();
            }

            @Override
            public void onFailure(Call<List<SchoolSATData>> call, Throwable t) {
            }
        });
    }

    class cachedSchoolListTask extends AsyncTask{

        private final OnFinishedListener onFinishedListener;
        private final Context context;
        private final Throwable throwable;
        private List<School> schools;

        public cachedSchoolListTask(final OnFinishedListener onFinishedListener, final Context context,Throwable t) {
            this.onFinishedListener = onFinishedListener;
            this.context = context;
            this.throwable = t;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            schools = SchoolRoomDatabase.getDatabase(context).schoolDao().getAllSchools();
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if(schools.size() == 0) {
                onFinishedListener.onFailure(throwable);
            }else{
                onFinishedListener.onFinished(schools);
            }
        }
    }

    class insertSchoolListTask extends AsyncTask{

        private final OnFinishedListener onFinishedListener;
        private final Context context;
        private final Response<List<School>> response;

        public insertSchoolListTask(final OnFinishedListener onFinishedListener, final Context context,Response<List<School>> response) {
            this.onFinishedListener = onFinishedListener;
            this.context = context;
            this.response = response;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            SchoolRoomDatabase.getDatabase(context).schoolDao().insertSchools(response.body());
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            onFinishedListener.onFinished(response.body());
        }
    }

    class insertSchoolSATDataTask extends AsyncTask{

        private final OnFinishedListener onFinishedListener;
        private final Context context;
        private final Response<List<SchoolSATData>> response;

        public insertSchoolSATDataTask(final OnFinishedListener onFinishedListener, final Context context,Response<List<SchoolSATData>> response) {
            this.onFinishedListener = onFinishedListener;
            this.context = context;
            this.response = response;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            SchoolRoomDatabase.getDatabase(context).schoolDao().insertSchoolSATData(response.body());
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
        }
    }
}
