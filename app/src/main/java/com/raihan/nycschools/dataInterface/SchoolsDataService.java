package com.raihan.nycschools.dataInterface;

import com.raihan.nycschools.model.School;
import com.raihan.nycschools.model.SchoolSATData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SchoolsDataService {

    @GET("s3k6-pzi2.json")
    Call<List<School>> getSchoolListData();

    @GET("f9bf-2cp4.json")
    Call<List<SchoolSATData>> getSchoolSATData();

}
