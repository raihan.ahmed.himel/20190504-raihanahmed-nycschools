package com.raihan.nycschools.contract;

import android.content.Context;

import com.raihan.nycschools.model.School;
import com.raihan.nycschools.model.SchoolSATData;

import java.util.List;

/**
 * Created by Raihan on 05/04/19.
 */

public interface SchoolSATDataContract {

    /**
     * Call when user interact with the view and other when view OnDestroy()
     * */
    interface Presenter{

        void onDestroy();

        void onRefreshButtonClick();

        void requestDataFromServer();

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetSchoolSATADataInteractor class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDataView(SchoolSATData schoolSATData, School school);

        void onResponseFailure(Throwable throwable);

    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface Interactor {

        void getSATData(OnFinishedListener onFinishedListener, Context context, String dbn);

        interface OnFinishedListener {
            void onFinished(SchoolSATData schoolSATData, School school);
            void onFailure(School school);
        }
    }
}

