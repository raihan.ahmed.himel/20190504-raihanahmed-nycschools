package com.raihan.nycschools.contract;

import android.content.Context;

import com.raihan.nycschools.model.School;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raihan on 05/04/19.
 */

public interface SchoolListContract {

    /**
     * Call when user interact with the view and other when view OnDestroy()
     * */
    interface Presenter{

        void onDestroy();

        void onRefreshButtonClick();

        void requestDataFromServer();


    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetSchoolListInteractor class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDataToRecyclerView(List<School> schoolArrayList);

        void onResponseFailure(Throwable throwable);

        void launchDataActivity(String dbn);
    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface Interactor {

        interface OnFinishedListener {
            void onFinished(List<School> schoolArrayList);
            void onFailure(Throwable t);
        }

        void getSchoolList(OnFinishedListener onFinishedListener, Context context);
    }
}

