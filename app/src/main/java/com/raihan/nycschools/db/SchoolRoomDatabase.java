package com.raihan.nycschools.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.raihan.nycschools.model.School;
import com.raihan.nycschools.model.SchoolSATData;

/**
 * This is the backend. The database. This used to be done by the OpenHelper.
 */

@Database(entities = {School.class, SchoolSATData.class}, version = 1)
public abstract class SchoolRoomDatabase extends RoomDatabase {

    public abstract SchoolDao schoolDao();

    // marking the instance as volatile to ensure atomic access to the variable
    private static volatile SchoolRoomDatabase INSTANCE;

    public static SchoolRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (SchoolRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            SchoolRoomDatabase.class, "nyc_schools_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
