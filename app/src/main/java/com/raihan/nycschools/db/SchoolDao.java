package com.raihan.nycschools.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.raihan.nycschools.model.School;
import com.raihan.nycschools.model.SchoolSATData;

import java.util.List;

@Dao
public interface SchoolDao {

    @Query("SELECT * from School")
    List<School> getAllSchools();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSchools(List<School> schoolList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSchoolSATData(List<SchoolSATData> body);

    @Query("SELECT * FROM SchoolSATData WHERE dbn = :dbn")
    public List<SchoolSATData> findSchoolSATDataWithDbn(String dbn);

    @Query("SELECT * FROM School WHERE dbn = :dbn")
    public List<School> findSchoolWithDbn(String dbn);

    @Query("DELETE FROM school")
    void deleteAll();

}
