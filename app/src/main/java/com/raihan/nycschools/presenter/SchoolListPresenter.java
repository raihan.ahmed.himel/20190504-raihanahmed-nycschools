package com.raihan.nycschools.presenter;

import android.content.Context;

import com.raihan.nycschools.contract.SchoolListContract;
import com.raihan.nycschools.model.School;

import java.util.List;

public class SchoolListPresenter implements SchoolListContract.Presenter, SchoolListContract.Interactor.OnFinishedListener {

    private final SchoolListContract.Interactor getSchoolListInteractor;
    private final SchoolListContract.MainView mainView;
    private final Context context;

    public SchoolListPresenter(SchoolListContract.MainView mainView, SchoolListContract.Interactor getSchoolListInteractor, Context context) {
        this.mainView = mainView;
        this.getSchoolListInteractor = getSchoolListInteractor;
        this.context = context;
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onRefreshButtonClick() {

    }

    @Override
    public void requestDataFromServer() {
        getSchoolListInteractor.getSchoolList(this,context);
    }

    @Override
    public void onFinished(List<School> schoolArrayList) {
        mainView.setDataToRecyclerView(schoolArrayList);
    }

    @Override
    public void onFailure(Throwable t) {
        mainView.onResponseFailure(t);
    }
}
