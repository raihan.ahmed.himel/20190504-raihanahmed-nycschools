package com.raihan.nycschools.presenter;

import android.content.Context;
import com.raihan.nycschools.contract.SchoolSATDataContract;
import com.raihan.nycschools.model.School;
import com.raihan.nycschools.model.SchoolSATData;

public class SchoolSATDataPresenter implements SchoolSATDataContract.Presenter, SchoolSATDataContract.Interactor.OnFinishedListener {

    private final SchoolSATDataContract.Interactor getSchoolListInteractor;
    private final SchoolSATDataContract.MainView mainView;
    private final Context context;
    private String dbn;

    public SchoolSATDataPresenter(SchoolSATDataContract.MainView mainView, SchoolSATDataContract.Interactor getSchoolListInteractor, Context context, String dbn) {
        this.mainView = mainView;
        this.getSchoolListInteractor = getSchoolListInteractor;
        this.context = context;
        this.dbn = dbn;
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onRefreshButtonClick() {

    }

    @Override
    public void requestDataFromServer() {
        getSchoolListInteractor.getSATData(this,context,dbn);
    }


    @Override
    public void onFinished(SchoolSATData schoolSATData, School school) {
        mainView.setDataView(schoolSATData,school);
    }

    @Override
    public void onFailure(School school) {
        mainView.setDataView(null,school);
    }
}
