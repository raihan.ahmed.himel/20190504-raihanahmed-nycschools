package com.raihan.nycschools.adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.raihan.nycschools.R;
import com.raihan.nycschools.contract.SchoolListContract;
import com.raihan.nycschools.model.School;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListAdapter.SchoolViewHolder> implements Filterable {

    private final SchoolListContract.MainView view;
    private List<School> dataList;
    private List<School> mfilteredList;

    public SchoolListAdapter(List<School> dataList, SchoolListContract.MainView view) {
        this.dataList = dataList;
        this.mfilteredList = dataList;
        this.view = view;
    }


    @Override
    public SchoolViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.single_view_row, parent, false);
        return new SchoolViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SchoolViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.txtNoticeTitle.setText(mfilteredList.get(position).getSchool_name());
        if(mfilteredList.get(position).getLocation().contains("(")) {
            holder.txtNoticeBrief.setText(Html.fromHtml("<font color=#2B7629>" + "Address: "+"</font>" + mfilteredList.get(position).getLocation().substring(0, mfilteredList.get(position).getLocation().indexOf("("))));
        }
        holder.txtNoticeFilePath.setText(Html.fromHtml("<font color=#3F7AA9>" + "Website: "+"</font>"+mfilteredList.get(position).getWebsite()));
        holder.txt_phone.setText(Html.fromHtml("<font color=#8A2433>" + "Phone: "+"</font>"+mfilteredList.get(position).getPhone_number()));
        holder.txt_email.setText(Html.fromHtml("<font color=#BBB553>" + "Email: "+"</font>"+mfilteredList.get(position).getSchool_email()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.launchDataActivity(mfilteredList.get(position).getDbn());
            }
        });

        borderColor(position,holder.borderview);
    }

    private void borderColor(int position, View borderview) {
        if (position%4 == 0){
            borderview.setBackgroundColor(Color.parseColor("#1e86cf"));
        } else if (position%4 == 1){
            borderview.setBackgroundColor(Color.parseColor("#2ca0ea"));
        } else if (position%4 == 2){
            borderview.setBackgroundColor(Color.parseColor("#2cc4ea"));
        } else if (position%4 == 3){
            borderview.setBackgroundColor(Color.parseColor("#2ceae3"));
        }
    }

    @Override
    public int getItemCount() {
        return mfilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                List<School> resultFilteredList;
                if (charString.isEmpty()) {

                    resultFilteredList = dataList;
                } else {

                    ArrayList<School> filteredList = new ArrayList<>();

                    for (School school : dataList) {

                        if(school.getSchool_name().toLowerCase().contains(charString.toLowerCase())){
                            filteredList.add(school);
                        }
                    }

                    resultFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = resultFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mfilteredList = (ArrayList<School>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class SchoolViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_notice_title) TextView txtNoticeTitle;
        @BindView(R.id.txt_notice_brief) TextView txtNoticeBrief;
        @BindView(R.id.txt_notice_file_path) TextView txtNoticeFilePath;
        @BindView(R.id.txt_phone) TextView txt_phone;
        @BindView(R.id.txt_email) TextView txt_email;
        @BindView(R.id.borderview) View borderview;

        SchoolViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}