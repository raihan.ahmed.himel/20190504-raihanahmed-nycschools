package com.raihan.nycschools.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/*
    should have been done using kotlin data class
 */


@Entity(tableName = "School")
public class School {

    @ColumnInfo(name="academicopportunities1")
    @SerializedName("academicopportunities1")
    private String academicopportunities1 ;
    @ColumnInfo(name="academicopportunities2")
    @SerializedName("academicopportunities2")
    private String academicopportunities2 ;
    @ColumnInfo(name="admissionspriority11")
    @SerializedName("admissionspriority11")
    private String admissionspriority11 ;
    @ColumnInfo(name="admissionspriority21")
    @SerializedName("admissionspriority21")
    private String admissionspriority21 ;
    @ColumnInfo(name="admissionspriority31")
    @SerializedName("admissionspriority31")
    private String admissionspriority31 ;
    @ColumnInfo(name="attendance_rate")
    @SerializedName("attendance_rate")
    private String attendance_rate ;
    @ColumnInfo(name="bbl")
    @SerializedName("bbl")
    private String bbl ;
    @ColumnInfo(name="bin")
    @SerializedName("bin")
    private String bin ;
    @ColumnInfo(name="boro")
    @SerializedName("boro")
    private String boro ;
    @ColumnInfo(name="borough")
    @SerializedName("borough")
    private String borough ;
    @ColumnInfo(name="building_code")
    @SerializedName("building_code")
    private String building_code ;
    @ColumnInfo(name="bus")
    @SerializedName("bus")
    private String bus ;
    @ColumnInfo(name="census_tract")
    @SerializedName("census_tract")
    private String census_tract ;
    @ColumnInfo(name="city")
    @SerializedName("city")
    private String city ;
    @ColumnInfo(name="code1")
    @SerializedName("code1")
    private String code1 ;
    @ColumnInfo(name="community_board")
    @SerializedName("community_board")
    private String community_board ;
    @ColumnInfo(name="council_district")
    @SerializedName("council_district")
    private String council_district ;
    @ColumnInfo(name="dbn")
    @PrimaryKey
    @NonNull
    @SerializedName("dbn")
    private String dbn ;
    @ColumnInfo(name="directions1")
    @SerializedName("directions1")
    private String directions1 ;
    @ColumnInfo(name="ell_programs")
    @SerializedName("ell_programs")
    private String ell_programs ;
    @ColumnInfo(name="extracurricular_activities")
    @SerializedName("extracurricular_activities")
    private String extracurricular_activities ;
    @ColumnInfo(name="fax_number")
    @SerializedName("fax_number")
    private String fax_number ;
    @ColumnInfo(name="finalgrades")
    @SerializedName("finalgrades")
    private String finalgrades ;
    @ColumnInfo(name="grade9geapplicants1")
    @SerializedName("grade9geapplicants1")
    private String grade9geapplicants1 ;
    @ColumnInfo(name="grade9geapplicantsperseat1")
    @SerializedName("grade9geapplicantsperseat1")
    private String grade9geapplicantsperseat1 ;
    @ColumnInfo(name="grade9gefilledflag1")
    @SerializedName("grade9gefilledflag1")
    private String grade9gefilledflag1 ;
    @ColumnInfo(name="grade9swdapplicants1")
    @SerializedName("grade9swdapplicants1")
    private String grade9swdapplicants1 ;
    @ColumnInfo(name="grade9swdapplicantsperseat1")
    @SerializedName("grade9swdapplicantsperseat1")
    private String grade9swdapplicantsperseat1 ;
    @ColumnInfo(name="grade9swdfilledflag1")
    @SerializedName("grade9swdfilledflag1")
    private String grade9swdfilledflag1 ;
    @ColumnInfo(name="grades2018")
    @SerializedName("grades2018")
    private String grades2018 ;
    @ColumnInfo(name="interest1")
    @SerializedName("interest1")
    private String interest1 ;
    @ColumnInfo(name="latitude")
    @SerializedName("latitude")
    private String latitude ;
    @ColumnInfo(name="location")
    @SerializedName("location")
    private String location ;
    @ColumnInfo(name="longitude")
    @SerializedName("longitude")
    private String longitude ;
    @ColumnInfo(name="method1")
    @SerializedName("method1")
    private String method1 ;
    @ColumnInfo(name="neighborhood")
    @SerializedName("neighborhood")
    private String neighborhood ;
    @ColumnInfo(name="nta")
    @SerializedName("nta")
    private String nta ;
    @ColumnInfo(name="offer_rate1")
    @SerializedName("offer_rate1")
    private String offer_rate1 ;
    @ColumnInfo(name="overview_paragraph")
    @SerializedName("overview_paragraph")
    private String overview_paragraph ;
    @ColumnInfo(name="pct_stu_enough_variety")
    @SerializedName("pct_stu_enough_variety")
    private String pct_stu_enough_variety ;
    @ColumnInfo(name="pct_stu_safe")
    @SerializedName("pct_stu_safe")
    private String pct_stu_safe ;
    @ColumnInfo(name="phone_number")
    @SerializedName("phone_number")
    private String phone_number ;
    @ColumnInfo(name="primary_address_line_1")
    @SerializedName("primary_address_line_1")
    private String primary_address_line_1 ;
    @ColumnInfo(name="program1")
    @SerializedName("program1")
    private String program1 ;
    @ColumnInfo(name="requirement1_1")
    @SerializedName("requirement1_1")
    private String requirement1_1 ;
    @ColumnInfo(name="requirement2_1")
    @SerializedName("requirement2_1")
    private String requirement2_1 ;
    @ColumnInfo(name="requirement3_1")
    @SerializedName("requirement3_1")
    private String requirement3_1 ;
    @ColumnInfo(name="requirement4_1")
    @SerializedName("requirement4_1")
    private String requirement4_1 ;
    @ColumnInfo(name="requirement5_1")
    @SerializedName("requirement5_1")
    private String requirement5_1 ;
    @ColumnInfo(name="school_10th_seats")
    @SerializedName("school_10th_seats")
    private String school_10th_seats ;
    @ColumnInfo(name="school_accessibility_description")
    @SerializedName("school_accessibility_description")
    private String school_accessibility_description ;
    @ColumnInfo(name="school_email")
    @SerializedName("school_email")
    private String school_email ;
    @ColumnInfo(name="school_name")
    @SerializedName("school_name")
    private String school_name ;
    @ColumnInfo(name="school_sports")
    @SerializedName("school_sports")
    private String school_sports ;
    @ColumnInfo(name="seats101")
    @SerializedName("seats101")
    private String seats101 ;
    @ColumnInfo(name="seats9ge1")
    @SerializedName("seats9ge1")
    private String seats9ge1 ;
    @ColumnInfo(name="seats9swd1")
    @SerializedName("seats9swd1")
    private String seats9swd1 ;
    @ColumnInfo(name="state_code")
    @SerializedName("state_code")
    private String state_code ;
    @ColumnInfo(name="subway")
    @SerializedName("subway")
    private String subway ;
    @ColumnInfo(name="total_students")
    @SerializedName("total_students")
    private String total_students ;
    @ColumnInfo(name="website")
    @SerializedName("website")
    private String website ;
    @ColumnInfo(name="zip")
    @SerializedName("zip")
    private String zip ;

    public School(String academicopportunities1, String academicopportunities2, String admissionspriority11, String admissionspriority21, String admissionspriority31, String attendance_rate, String bbl, String bin, String boro, String borough, String building_code, String bus, String census_tract, String city, String code1, String community_board, String council_district, String dbn, String directions1, String ell_programs, String extracurricular_activities, String fax_number, String finalgrades, String grade9geapplicants1, String grade9geapplicantsperseat1, String grade9gefilledflag1, String grade9swdapplicants1, String grade9swdapplicantsperseat1, String grade9swdfilledflag1, String grades2018, String interest1, String latitude, String location, String longitude, String method1, String neighborhood, String nta, String offer_rate1, String overview_paragraph, String pct_stu_enough_variety, String pct_stu_safe, String phone_number, String primary_address_line_1, String program1, String requirement1_1, String requirement2_1, String requirement3_1, String requirement4_1, String requirement5_1, String school_10th_seats, String school_accessibility_description, String school_email, String school_name, String school_sports, String seats101, String seats9ge1, String seats9swd1, String state_code, String subway, String total_students, String website, String zip) {
        this.academicopportunities1 = academicopportunities1;
        this.academicopportunities2 = academicopportunities2;
        this.admissionspriority11 = admissionspriority11;
        this.admissionspriority21 = admissionspriority21;
        this.admissionspriority31 = admissionspriority31;
        this.attendance_rate = attendance_rate;
        this.bbl = bbl;
        this.bin = bin;
        this.boro = boro;
        this.borough = borough;
        this.building_code = building_code;
        this.bus = bus;
        this.census_tract = census_tract;
        this.city = city;
        this.code1 = code1;
        this.community_board = community_board;
        this.council_district = council_district;
        this.dbn = dbn;
        this.directions1 = directions1;
        this.ell_programs = ell_programs;
        this.extracurricular_activities = extracurricular_activities;
        this.fax_number = fax_number;
        this.finalgrades = finalgrades;
        this.grade9geapplicants1 = grade9geapplicants1;
        this.grade9geapplicantsperseat1 = grade9geapplicantsperseat1;
        this.grade9gefilledflag1 = grade9gefilledflag1;
        this.grade9swdapplicants1 = grade9swdapplicants1;
        this.grade9swdapplicantsperseat1 = grade9swdapplicantsperseat1;
        this.grade9swdfilledflag1 = grade9swdfilledflag1;
        this.grades2018 = grades2018;
        this.interest1 = interest1;
        this.latitude = latitude;
        this.location = location;
        this.longitude = longitude;
        this.method1 = method1;
        this.neighborhood = neighborhood;
        this.nta = nta;
        this.offer_rate1 = offer_rate1;
        this.overview_paragraph = overview_paragraph;
        this.pct_stu_enough_variety = pct_stu_enough_variety;
        this.pct_stu_safe = pct_stu_safe;
        this.phone_number = phone_number;
        this.primary_address_line_1 = primary_address_line_1;
        this.program1 = program1;
        this.requirement1_1 = requirement1_1;
        this.requirement2_1 = requirement2_1;
        this.requirement3_1 = requirement3_1;
        this.requirement4_1 = requirement4_1;
        this.requirement5_1 = requirement5_1;
        this.school_10th_seats = school_10th_seats;
        this.school_accessibility_description = school_accessibility_description;
        this.school_email = school_email;
        this.school_name = school_name;
        this.school_sports = school_sports;
        this.seats101 = seats101;
        this.seats9ge1 = seats9ge1;
        this.seats9swd1 = seats9swd1;
        this.state_code = state_code;
        this.subway = subway;
        this.total_students = total_students;
        this.website = website;
        this.zip = zip;
    }

    public School() {
    }

    public String getAcademicopportunities1() {
        return academicopportunities1;
    }

    public void setAcademicopportunities1(String academicopportunities1) {
        this.academicopportunities1 = academicopportunities1;
    }

    public String getAcademicopportunities2() {
        return academicopportunities2;
    }

    public void setAcademicopportunities2(String academicopportunities2) {
        this.academicopportunities2 = academicopportunities2;
    }

    public String getAdmissionspriority11() {
        return admissionspriority11;
    }

    public void setAdmissionspriority11(String admissionspriority11) {
        this.admissionspriority11 = admissionspriority11;
    }

    public String getAdmissionspriority21() {
        return admissionspriority21;
    }

    public void setAdmissionspriority21(String admissionspriority21) {
        this.admissionspriority21 = admissionspriority21;
    }

    public String getAdmissionspriority31() {
        return admissionspriority31;
    }

    public void setAdmissionspriority31(String admissionspriority31) {
        this.admissionspriority31 = admissionspriority31;
    }

    public String getAttendance_rate() {
        return attendance_rate;
    }

    public void setAttendance_rate(String attendance_rate) {
        this.attendance_rate = attendance_rate;
    }

    public String getBbl() {
        return bbl;
    }

    public void setBbl(String bbl) {
        this.bbl = bbl;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getBoro() {
        return boro;
    }

    public void setBoro(String boro) {
        this.boro = boro;
    }

    public String getBorough() {
        return borough;
    }

    public void setBorough(String borough) {
        this.borough = borough;
    }

    public String getBuilding_code() {
        return building_code;
    }

    public void setBuilding_code(String building_code) {
        this.building_code = building_code;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public String getCensus_tract() {
        return census_tract;
    }

    public void setCensus_tract(String census_tract) {
        this.census_tract = census_tract;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCode1() {
        return code1;
    }

    public void setCode1(String code1) {
        this.code1 = code1;
    }

    public String getCommunity_board() {
        return community_board;
    }

    public void setCommunity_board(String community_board) {
        this.community_board = community_board;
    }

    public String getCouncil_district() {
        return council_district;
    }

    public void setCouncil_district(String council_district) {
        this.council_district = council_district;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getDirections1() {
        return directions1;
    }

    public void setDirections1(String directions1) {
        this.directions1 = directions1;
    }

    public String getEll_programs() {
        return ell_programs;
    }

    public void setEll_programs(String ell_programs) {
        this.ell_programs = ell_programs;
    }

    public String getExtracurricular_activities() {
        return extracurricular_activities;
    }

    public void setExtracurricular_activities(String extracurricular_activities) {
        this.extracurricular_activities = extracurricular_activities;
    }

    public String getFax_number() {
        return fax_number;
    }

    public void setFax_number(String fax_number) {
        this.fax_number = fax_number;
    }

    public String getFinalgrades() {
        return finalgrades;
    }

    public void setFinalgrades(String finalgrades) {
        this.finalgrades = finalgrades;
    }

    public String getGrade9geapplicants1() {
        return grade9geapplicants1;
    }

    public void setGrade9geapplicants1(String grade9geapplicants1) {
        this.grade9geapplicants1 = grade9geapplicants1;
    }

    public String getGrade9geapplicantsperseat1() {
        return grade9geapplicantsperseat1;
    }

    public void setGrade9geapplicantsperseat1(String grade9geapplicantsperseat1) {
        this.grade9geapplicantsperseat1 = grade9geapplicantsperseat1;
    }

    public String getGrade9gefilledflag1() {
        return grade9gefilledflag1;
    }

    public void setGrade9gefilledflag1(String grade9gefilledflag1) {
        this.grade9gefilledflag1 = grade9gefilledflag1;
    }

    public String getGrade9swdapplicants1() {
        return grade9swdapplicants1;
    }

    public void setGrade9swdapplicants1(String grade9swdapplicants1) {
        this.grade9swdapplicants1 = grade9swdapplicants1;
    }

    public String getGrade9swdapplicantsperseat1() {
        return grade9swdapplicantsperseat1;
    }

    public void setGrade9swdapplicantsperseat1(String grade9swdapplicantsperseat1) {
        this.grade9swdapplicantsperseat1 = grade9swdapplicantsperseat1;
    }

    public String getGrade9swdfilledflag1() {
        return grade9swdfilledflag1;
    }

    public void setGrade9swdfilledflag1(String grade9swdfilledflag1) {
        this.grade9swdfilledflag1 = grade9swdfilledflag1;
    }

    public String getGrades2018() {
        return grades2018;
    }

    public void setGrades2018(String grades2018) {
        this.grades2018 = grades2018;
    }

    public String getInterest1() {
        return interest1;
    }

    public void setInterest1(String interest1) {
        this.interest1 = interest1;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMethod1() {
        return method1;
    }

    public void setMethod1(String method1) {
        this.method1 = method1;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getNta() {
        return nta;
    }

    public void setNta(String nta) {
        this.nta = nta;
    }

    public String getOffer_rate1() {
        return offer_rate1;
    }

    public void setOffer_rate1(String offer_rate1) {
        this.offer_rate1 = offer_rate1;
    }

    public String getOverview_paragraph() {
        return overview_paragraph;
    }

    public void setOverview_paragraph(String overview_paragraph) {
        this.overview_paragraph = overview_paragraph;
    }

    public String getPct_stu_enough_variety() {
        return pct_stu_enough_variety;
    }

    public void setPct_stu_enough_variety(String pct_stu_enough_variety) {
        this.pct_stu_enough_variety = pct_stu_enough_variety;
    }

    public String getPct_stu_safe() {
        return pct_stu_safe;
    }

    public void setPct_stu_safe(String pct_stu_safe) {
        this.pct_stu_safe = pct_stu_safe;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getPrimary_address_line_1() {
        return primary_address_line_1;
    }

    public void setPrimary_address_line_1(String primary_address_line_1) {
        this.primary_address_line_1 = primary_address_line_1;
    }

    public String getProgram1() {
        return program1;
    }

    public void setProgram1(String program1) {
        this.program1 = program1;
    }

    public String getRequirement1_1() {
        return requirement1_1;
    }

    public void setRequirement1_1(String requirement1_1) {
        this.requirement1_1 = requirement1_1;
    }

    public String getRequirement2_1() {
        return requirement2_1;
    }

    public void setRequirement2_1(String requirement2_1) {
        this.requirement2_1 = requirement2_1;
    }

    public String getRequirement3_1() {
        return requirement3_1;
    }

    public void setRequirement3_1(String requirement3_1) {
        this.requirement3_1 = requirement3_1;
    }

    public String getRequirement4_1() {
        return requirement4_1;
    }

    public void setRequirement4_1(String requirement4_1) {
        this.requirement4_1 = requirement4_1;
    }

    public String getRequirement5_1() {
        return requirement5_1;
    }

    public void setRequirement5_1(String requirement5_1) {
        this.requirement5_1 = requirement5_1;
    }

    public String getSchool_10th_seats() {
        return school_10th_seats;
    }

    public void setSchool_10th_seats(String school_10th_seats) {
        this.school_10th_seats = school_10th_seats;
    }

    public String getSchool_accessibility_description() {
        return school_accessibility_description;
    }

    public void setSchool_accessibility_description(String school_accessibility_description) {
        this.school_accessibility_description = school_accessibility_description;
    }

    public String getSchool_email() {
        return school_email;
    }

    public void setSchool_email(String school_email) {
        this.school_email = school_email;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getSchool_sports() {
        return school_sports;
    }

    public void setSchool_sports(String school_sports) {
        this.school_sports = school_sports;
    }

    public String getSeats101() {
        return seats101;
    }

    public void setSeats101(String seats101) {
        this.seats101 = seats101;
    }

    public String getSeats9ge1() {
        return seats9ge1;
    }

    public void setSeats9ge1(String seats9ge1) {
        this.seats9ge1 = seats9ge1;
    }

    public String getSeats9swd1() {
        return seats9swd1;
    }

    public void setSeats9swd1(String seats9swd1) {
        this.seats9swd1 = seats9swd1;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getSubway() {
        return subway;
    }

    public void setSubway(String subway) {
        this.subway = subway;
    }

    public String getTotal_students() {
        return total_students;
    }

    public void setTotal_students(String total_students) {
        this.total_students = total_students;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
