package com.raihan.nycschools.view;

import android.location.Address;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.raihan.nycschools.R;
import com.raihan.nycschools.contract.SchoolSATDataContract;
import com.raihan.nycschools.interactor.GetSchoolSATDataInteractor;
import com.raihan.nycschools.model.School;
import com.raihan.nycschools.model.SchoolSATData;
import com.raihan.nycschools.presenter.SchoolSATDataPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SchoolSATDATAActivity extends AppCompatActivity implements SchoolSATDataContract.MainView {

    private SchoolSATDataContract.Presenter presenter;
    @BindView(R.id.schoolNameTextView) TextView schoolNameTextView;
    @BindView(R.id.schoolSATTextView) TextView schoolSATTextView;
    @BindView(R.id.schoolSATWritingTextView) TextView schoolSATWritingTextView;
    @BindView(R.id.schoolSATReadingTextView) TextView schoolSATReadingTextView;
    @BindView(R.id.schoolSATMathTextView) TextView schoolSATMathTextView;
    @BindView(R.id.schoolOverViewParagraphView) TextView schoolOverViewParagraphView;
    @BindView(R.id.schoolAcademicOppurtunitiesView) TextView schoolAcademicOppurtunitiesView;
    @BindView(R.id.schoolEmailView) TextView schoolEmailView;
    @BindView(R.id.schoolPhoneView) TextView schoolPhoneView;
    @BindView(R.id.schoolWebsiteView) TextView schoolWebsiteView;
    @BindView(R.id.schoolLocationView) TextView schoolLocationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_satdata);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);


        String dbn = getIntent().getStringExtra("dbn");
        presenter = new SchoolSATDataPresenter(this, new GetSchoolSATDataInteractor(),this,dbn);
        presenter.requestDataFromServer();

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setDataView(SchoolSATData schoolSATData, School school) {
        if(schoolSATData != null) {
            schoolNameTextView.setText(schoolSATData.getSchool_name());
            schoolSATMathTextView.setText("Average SAT Mathematics Score: "+schoolSATData.getSat_math_avg_score());
            schoolSATReadingTextView.setText("Average SAT Critical Reading Score: "+schoolSATData.getSat_critical_reading_avg_score());
            schoolSATWritingTextView.setText("Average SAT Writing Score: "+schoolSATData.getSat_writing_avg_score());
        }else{
            schoolNameTextView.setText(school.getSchool_name());
            schoolSATTextView.setText("SAT scores not available");
        }

        schoolOverViewParagraphView.setText(school.getOverview_paragraph());
        schoolAcademicOppurtunitiesView.setText(school.getAcademicopportunities1()
                +"/n"+school.getAcademicopportunities2());
        schoolEmailView.setText("Email: "+school.getSchool_email());
        schoolPhoneView.setText("Phone: "+school.getPhone_number());
        schoolWebsiteView.setText("Website: "+school.getWebsite());
        if(school.getLocation().contains("(")) {
            schoolLocationView.setText("Address: " + school.getLocation().substring(0, school.getLocation().indexOf("(")));
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {

    }
}
