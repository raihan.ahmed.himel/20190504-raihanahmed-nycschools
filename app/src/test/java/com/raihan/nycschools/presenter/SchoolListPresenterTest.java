package com.raihan.nycschools.presenter;

import android.content.Context;

import com.raihan.nycschools.contract.SchoolListContract;
import com.raihan.nycschools.model.School;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SchoolListPresenterTest {

    @Mock
    SchoolListPresenter schoolListPresenter;

    @Mock
    SchoolListContract.MainView view;

    @Mock
    SchoolListContract.Interactor interactor;

    @Mock
    Context context;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_SetUpIsWorking() {
        assertNotNull(schoolListPresenter);
    }

    @Test
    public void test_constructor() {
        SchoolListPresenter presenter = Mockito.spy(new SchoolListPresenter(view,interactor,context));
        assertNotNull(presenter);
    }

    @Test
    public void test_requestDataFromServerCallsInteractorGetSchoolList(){
        SchoolListPresenter presenter = Mockito.spy(new SchoolListPresenter(view,interactor,context));
        presenter.requestDataFromServer();
        Mockito.verify(interactor,Mockito.atLeastOnce()).getSchoolList(org.mockito.ArgumentMatchers.any(SchoolListContract.Interactor.OnFinishedListener.class),org.mockito.ArgumentMatchers.any(Context.class));
    }
}