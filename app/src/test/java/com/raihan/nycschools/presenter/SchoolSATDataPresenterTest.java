package com.raihan.nycschools.presenter;

import android.content.Context;

import com.raihan.nycschools.contract.SchoolListContract;
import com.raihan.nycschools.contract.SchoolSATDataContract;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertNotNull;

public class SchoolSATDataPresenterTest {

    @Mock
    SchoolSATDataPresenter schoolSATDataPresenter;

    @Mock
    SchoolSATDataContract.MainView view;

    @Mock
    SchoolSATDataContract.Interactor interactor;

    @Mock
    Context context;


    String dbn = "";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_SetUpIsWorking() {
        assertNotNull(schoolSATDataPresenter);
    }

    @Test
    public void test_constructor() {
        SchoolSATDataPresenter presenter = Mockito.spy(new SchoolSATDataPresenter(view,interactor,context,dbn));
        assertNotNull(presenter);
    }

    @Test
    public void test_requestDataFromServerCallsInteractorGetSchoolList(){
        SchoolSATDataPresenter presenter = Mockito.spy(new SchoolSATDataPresenter(view,interactor,context,dbn));
        presenter.requestDataFromServer();
        Mockito.verify(interactor,Mockito.atLeastOnce()).getSATData(ArgumentMatchers.any(SchoolSATDataContract.Interactor.OnFinishedListener.class), ArgumentMatchers.any(Context.class),ArgumentMatchers.anyString());
    }
}