package com.raihan.nycschools.db;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.runner.AndroidJUnit4;

import com.raihan.nycschools.model.School;
import com.raihan.nycschools.model.SchoolSATData;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.test.core.app.ApplicationProvider;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class SchoolDaoTest {
    private SchoolDao schoolDao;
    private SchoolRoomDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, SchoolRoomDatabase.class).allowMainThreadQueries().build();
        schoolDao = db.schoolDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void writeSchoolAndReadInList() throws Exception {
        School school = new School();
        school.setSchool_name("school");
        school.setDbn("dbn");
        ArrayList<School> toInsert = new ArrayList<School>();
        toInsert.add(school);
        schoolDao.insertSchools(toInsert);
        List<School> byDbn = schoolDao.findSchoolWithDbn("dbn");
        assertThat(byDbn.get(0).getSchool_name(), equalTo(school.getSchool_name()));
    }

    @Test
    public void writeSchoolSATDataAndReadInList() throws Exception {
        SchoolSATData schoolSATData = new SchoolSATData();
        schoolSATData.setSchool_name("school");
        schoolSATData.setDbn("dbn");
        ArrayList<SchoolSATData> toInsert = new ArrayList<SchoolSATData>();
        toInsert.add(schoolSATData);
        schoolDao.insertSchoolSATData(toInsert);
        List<SchoolSATData> byDbn = schoolDao.findSchoolSATDataWithDbn("dbn");
        assertThat(byDbn.get(0).getSchool_name(), equalTo(schoolSATData.getSchool_name()));
    }
}